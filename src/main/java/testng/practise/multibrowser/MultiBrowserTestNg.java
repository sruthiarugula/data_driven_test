package testng.practise.multibrowser;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MultiBrowserTestNg {
	public WebDriver driver;
	public static final String USERNAME = "sruthiarugula";
	public static final String ACCESS_KEY = "d9e742cb-4b8c-4423-a4a3-350a9e42be6d";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";

	@Parameters("browser")

	@BeforeClass

	// Passing Browser parameter from TestNG xml

	public void beforeTest(String browser) {

		// If the browser is Firefox, then do this

		if (browser.equalsIgnoreCase("chrome")) {
			//System.setProperty("webdriver.chrome.driver", "C://driver//chromedriver.exe");
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			caps.setCapability("platform", "Windows 10");
			caps.setCapability("version", "43.0");
			// caps.setCapability(capabilityName, value);
			// System.setProperty("webdriver.chrome.driver",
			// "C://driver//chromedriver.exe");
			try {
				driver = new RemoteWebDriver(new URL(URL), caps);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// If browser is IE, then do this

		else if (browser.equalsIgnoreCase("ie"))

		{

			// Here I am setting up the path for my IEDriver

			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			// caps.setCapability("platform", "Windows 10");
			// caps.setCapability("version", "43.0");
			// caps.setCapability(capabilityName, value);
			// System.setProperty("webdriver.chrome.driver",
			// "C://driver//chromedriver.exe");
			try {
				driver = new RemoteWebDriver(new URL(URL), caps);
			} catch (MalformedURLException e) {
			}
		} else if (browser.equalsIgnoreCase("firefox")) {

			DesiredCapabilities caps = DesiredCapabilities.firefox();
			// caps.setCapability("platform", "Windows 10");
			// caps.setCapability("version", "43.0");
			// caps.setCapability(capabilityName, value);
			// System.setProperty("webdriver.chrome.driver",
			// "C://driver//chromedriver.exe");
			try {
				driver = new RemoteWebDriver(new URL(URL), caps);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Test

	public void navigate() {
		driver.get("https://www.amazon.com");

	}

	@AfterClass
	public void afterTest() {
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		driver.quit();
		driver.close();
		// closeBrowser();

	}
}
