package testng.practise.parallel;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

//import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;

public class ParallelTestNg {
	public WebDriver driver;

	public static final String USERNAME = "sruthiarugula";
	public static final String ACCESS_KEY = "d9e742cb-4b8c-4423-a4a3-350a9e42be6d";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";

	@BeforeMethod
	public void beforeMethod() {

		DesiredCapabilities caps = DesiredCapabilities.chrome();
		caps.setCapability("platform", "Windows 10");
		caps.setCapability("version", "43.0");
		caps.setCapability("name", "datadrivenTestng");
		// caps.setCapability(capabilityName, value);
		// System.setProperty("webdriver.chrome.driver",
		// "C://driver//chromedriver.exe");
		try {
			driver = new RemoteWebDriver(new URL(URL), caps);
			driver.get("https://www.google.com/");
			System.out.println("I am from before method");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void launchGooglePage() {
		System.out.println("Hello I am trying to launch GoogleHomePage");
		driver.get("https://www.google.com/");
		 driver.quit();

	}

	
	 
	
	@Test

	@Parameters({ "searchInput" })

	public void test(String searchInput) {
		System.out.println(" in the parameterised testing");
		String currentUrl = driver.getCurrentUrl();
		System.out.println("teloing the current URl" + currentUrl);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		driver.findElement(By.id("lst-ib")).sendKeys(searchInput);
		//driver.findElement(By.xpath("//*[@id=\"sbtc\"]/div[2]/div[2]/div[1]/div/ul/li[11]/div")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	WebElement ele=	driver.findElement(By.xpath("//*[@id='sbtc']/div[2]/div[2]/div[1]/div/ul/li[13]/div/span[1]/span/input"));
	ele.click();
		
		

	}

	/*@AfterMethod
	public void searchResults() {
		// driver.close();
		driver.getCurrentUrl();

		driver.quit();
		// driver.close();
		// closeBrowser();
	}*/

	@AfterSuite
	public void afterMethod() {
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		//driver.quit();
		//driver.close();
	}

}
