package testng.practise.jsonfileconversion;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchInputClass {
	@JsonProperty
	private String Key;
	@JsonProperty
	private String searchValue;

}
