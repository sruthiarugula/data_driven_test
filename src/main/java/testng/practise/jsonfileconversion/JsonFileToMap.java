package testng.practise.jsonfileconversion;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;

public class JsonFileToMap {
	List<HashMap<String, String>> dataAsMap = null;
	String[] st;

	public String[] jsonData() {
		// public static void main(String[] args) {

		try {

			File f = new File("C:\\Capgemini\\user.json");

			byte[] mapData = Files.toByteArray(f);

			ObjectMapper objectMapper = new ObjectMapper();
			

			dataAsMap = objectMapper.readValue(mapData, List.class);
			st = new String[dataAsMap.size()];
			int i = 0;

			for (HashMap map : dataAsMap) {
				Set set = map.entrySet();
				Iterator iterator = set.iterator();
				while (iterator.hasNext()) {
					Map.Entry mentry = (Map.Entry) iterator.next();
					if (i < dataAsMap.size())
						st[i] = (String) mentry.getValue();
					i++;
				}

			}

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return st;
	}

}